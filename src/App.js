import './App.css';
import Login from './react-components/Login';
import Signup from './react-components/Signup';
import Logout from './react-components/Logout';
import Tablet from './react-components/Tablet';
import React from 'react';

class App extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      //middlewareAddress: 'https://sim-ebraille-server.netlify.app/api',
      middlewareAddress: 'https://sim-ebraille-server.herokuapp.com/api',
      loggedIn: false,
      email: '',
    };
    this.setLoggedOut = this.setLoggedOut.bind(this);
    this.setLoggedIn = this.setLoggedIn.bind(this);
  }
  
  setLoggedOut() {
    this.setState({
      loggedIn: false,
      email: '',
    });
  }

  setLoggedIn(mail) {
    this.setState({
      loggedIn: true,
      email: mail,
    });
  }

  render(){
    let logo = '../imgsBraille/e.webp';

    let regLogIn = <div>
      <Login
        loggedIn={this.state.loggedIn}
        setLoggedIn={this.setLoggedIn}
        setLoggedOut={this.setLoggedOut}
        middlewareAddress={this.state.middlewareAddress}
      />
      <Signup 
        loggedIn={this.state.loggedIn}
        setLoggedIn={this.setLoggedIn}
        setLoggedOut={this.setLoggedOut}
        middlewareAddress={this.state.middlewareAddress}
      />
      </div>

    return(
      <div>
        <nav className="navbar navbar-light bg-light">
          <a className="navbar-brand" href="/">
            <img src={logo} width="30" height="30" className="d-inline-block align-top" alt="" />
            eBraille-simulator
          </a>
            { !this.state.loggedIn ? regLogIn : <Logout loggedIn={this.state.loggedIn} setLoggedIn={this.setLoggedIn} setLoggedOut={this.setLoggedOut}/> }
        </nav>

        <div className='container'>
          
          <Tablet 
            loggedIn={this.state.loggedIn}
            setLoggedIn={this.setLoggedIn}
            setLoggedOut={this.setLoggedOut}
            email={this.state.email}
            middlewareAddress={this.state.middlewareAddress}
          />
        </div>
    </div>
      
    );
  }
}
export default App;