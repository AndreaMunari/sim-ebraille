import React from 'react';
//import './LetteraBraille.css';

class LetteraBraille extends React.Component {
/*
    constructor(props) {
        super(props);
        this.state = {
          contaApici: 0
        };
    
    }
*/
    isUpper(str){
        return !/[a-z]/.test(str) && /[A-Z]/.test(str);
    }

    isLetter(str){
        let lettere = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyzèéùàò";
        
        return lettere.includes(str);
    }

    isNumber(str){
        let numeri = ['1','2','3','4','5','6','7','8','9','0']; 
        
        return numeri.includes(str);
    }


    render() {

        let lettera = this.props.lettera;
        let prec = this.props.precedente;//carattere che ha preceduto la lettera corrente
        let immaginiRichiesta = ['../imgsBraille/', 'null'];

        if(this.isLetter(lettera)){
            if(this.isUpper(lettera)) {
                immaginiRichiesta[0]+='maiuscolo';
                immaginiRichiesta[1] = '../imgsBraille/'+lettera;
            } else if (!this.isUpper(lettera)) {
                immaginiRichiesta[0] = '../imgsBraille/'+lettera.toUpperCase();
            }
        } else if (this.isNumber(lettera)) {//se numero
            let convertita = ' ';

            if(lettera==='1'){
                convertita = 'a';
            } else if(lettera==='2'){
                convertita = 'b';
            } else if(lettera==='3'){
                convertita = 'c';
            } else if(lettera==='4'){
                convertita = 'd';
            } else if(lettera==='5'){
                convertita = 'e';
            } else if(lettera==='6'){
                convertita = 'f';
            } else if(lettera==='7'){
                convertita = 'g';
            } else if(lettera==='8'){
                convertita = 'h';
            } else if (lettera==='9') {
                convertita = 'i';
            } else if (lettera==='0') {
                convertita = 'j';
            }

            //console.log('conta numeri:');
            //console.log(this.state.contaNumeri);
            if(!this.isNumber(prec)){//se il char precedente NON era un numero, serve il segnanumero
                //let nuovoContaNumeri = this.state.contaNumeri+1;
                //this.setState({contaNumeri: nuovoContaNumeri});
                immaginiRichiesta[0]+='segna-numero';
                immaginiRichiesta[1] = '../imgsBraille/'+convertita.toUpperCase();
            } else {//altrimenti no
                //let nuovoContaNumeri = this.state.contaNumeri+1;
                //this.setState({contaNumeri: nuovoContaNumeri});
                immaginiRichiesta[0] = '../imgsBraille/'+convertita.toUpperCase();
            }
        } else {//se simbolo
            if(lettera === ' ' || lettera=== '\n' || lettera==='\r'){
                immaginiRichiesta[0]+='vuoto';
                //this.setState({contaNumeri: 0});//reset contanumeri
            } else if(lettera === "'" || lettera === '"'|| lettera==='`'){//apostrofo anche se si indica doppio apice
                immaginiRichiesta[0]+='apostrofo';
                //this.setState({contaNumeri: 0});//reset contanumeri
            } else if(lettera === '*') {
                immaginiRichiesta[0]+='asterisco';
                //this.setState({contaNumeri: 0});//reset contanumeri
            } else if (lettera === ':'){
                immaginiRichiesta[0]+='duepunti';
                //this.setState({contaNumeri: 0});//reset contanumeri
            } else if (lettera==='!') {
                immaginiRichiesta[0]+='esclamativo';
                //this.setState({contaNumeri: 0});//reset contanumeri
            } else if (lettera==='?') {
                immaginiRichiesta[0]+='interrogativo';
                //this.setState({contaNumeri: 0});//reset contanumeri
            } else if (lettera==='>') {
                immaginiRichiesta[0]+='maggiore';
                //this.setState({contaNumeri: 0});//reset contanumeri
            } else if (lettera==='<') {
                immaginiRichiesta[0]+='minore';
                //this.setState({contaNumeri: 0});//reset contanumeri
            } else if(lettera==='*'){
                immaginiRichiesta[0]+='per';
                //this.setState({contaNumeri: 0});//reset contanumeri
            } else if(lettera==="."){
                immaginiRichiesta[0]+='punto';
                //this.setState({contaNumeri: 0});//reset contanumeri (?)
            } else if (lettera === ';') {
                immaginiRichiesta[0]+='puntoevirgola';
                //this.setState({contaNumeri: 0});//reset contanumeri
            } else if (lettera === ',') {
                immaginiRichiesta[0]+='virgola';
               // this.setState({contaNumeri: 0});//reset contanumeri
            } /*else if (lettera === '"') {
                if(this.state.contaApici===0){
    
                    immaginiRichiesta[0]+='virgolettebasseAPERTE';
    
                    let nuovoContaApici = this.state.contaApici+1;
                    this.setState({contaApici: nuovoContaApici});
                } else {
                    immaginiRichiesta[0]+='virgolettebasseCHIUSE';
                    this.setState({contaApici: 0});
                }
                
                //this.setState({contaNumeri: 0});//reset contanumeri
    
                //!isNaN(Number(lettera))
            }*/ else if (lettera==='«') {
                immaginiRichiesta[0]+='virgolettebasseAPERTE';
            } else if (lettera==='»'){
                immaginiRichiesta[0]+='virgolettebasseCHIUSE';
            } else if (lettera==='기') {//carattere di riempimento
                //console.log("carattere aggiunto");
            } else if (lettera==='/' || lettera==='\\'){//barra in un verso o l'altro diventa sempre /
                immaginiRichiesta[0]+='barra';
            } else if (lettera==='-') {
                immaginiRichiesta[0]+='dash';
            } else if (lettera==='{') {
                immaginiRichiesta[0]+='{';
            } else if (lettera==='}') {
                immaginiRichiesta[0]+='}';
            } else if (lettera==='=') {
                immaginiRichiesta[0]+='=';
            } else if (lettera==='#'){
                immaginiRichiesta[0]+='segna-numero';
            } else if (lettera==='[') {
                immaginiRichiesta[0]+='[';
            } else if (lettera===']') {
                immaginiRichiesta[0]+=']';
            } else if (lettera==='(') {
                immaginiRichiesta[0]+='(';
            } else if (lettera===')') {
                immaginiRichiesta[0]+=')';
            } else if (lettera==='~') {
                immaginiRichiesta[0]+='segna-tilde';
                immaginiRichiesta[1]='../imgsBraille/'+'(';
            }
        }


        //fine controlli
        //completo il nome della/delle immagini richieste
        if(immaginiRichiesta[1]!=='null'){
            immaginiRichiesta[1] += '.webp';//.webp
        }
        immaginiRichiesta[0] += '.webp';//.webp
        //console.log(immaginiRichiesta);
        return (
            <>
                {immaginiRichiesta.map(item => {
                    if(item!=='null' && lettera!=='기') {
                        return <img src={item} key={item.id} className="letteraBraille" alt={lettera} />;
                    } else if (lettera===null){
                        return null;
                    } else {
                        return null;
                    }
                })}
            </>
            

            
        );
    }
}

export default LetteraBraille;