import 'bootstrap/dist/css/bootstrap.css';
import "bootstrap-icons/font/bootstrap-icons.css";
import Axios from 'axios';
import React from 'react';

class PageControls extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            menu: [],
            menuLabel: '',
            menuCurrentSel: 0,
            textToSpeech: window.speechSynthesis,
            voiceConfig: 1,
            listOfBooks: [],
            showBookId: 0,
            loading: false,//
        };
    
        this.handleMenu = this.handleMenu.bind(this);
        this.handleConfirm = this.handleConfirm.bind(this);
        this.handleUp = this.handleUp.bind(this);
        this.handleDown = this.handleDown.bind(this);
        this.generateSubMenu = this.generateSubMenu.bind(this);
        this.listBooks = this.listBooks.bind(this);
        this.handleDateTime = this.handleDateTime.bind(this);
        this.say = this.say.bind(this);
        this.configureVoice = this.configureVoice.bind(this);
    }

    reset(){
        /*Doesn't reset:
         - textToSpeech
         - voiceConfig
         - showBookId
        */
        this.setState({
            menuLabel: "",
            menu: [],
            menuCurrentSel: 0,
            loading: false,
        });
    }

    /*say something using the configured voice settings*/
    say(phrase){
        let config = this.state.voiceConfig;
        let pieces = phrase.split('\t');

        if(config === undefined){
            window.speechSynthesis.speak(new SpeechSynthesisUtterance(pieces[0]));
        } else {
            let utter = new SpeechSynthesisUtterance(pieces[0]);

            utter.rate = this.state.voiceConfig;
            //utter.rate = this.state.voiceConfig[0];
            //utter.pitch = this.state.voiceConfig[1];
            //utter.language could be a future feature
            this.state.textToSpeech.speak(utter);
        }
    }

    configureVoice(updatedVoiceConfig){
        this.setState({
            voiceConfig: updatedVoiceConfig,
        });
    }

    handleMenu(){
        this.state.textToSpeech.cancel();//stops any other voice playing
        this.props.handleReset();
        console.log("Menu requested");

        var menuText = "Sperimenta\nLibreria\nImpostazioni\nInformazioni dispositivo";

        console.log(menuText);

        this.props.printTesto(menuText);

        let opzioni = menuText.split('\n');
        let phrase = "Menu principale, "+opzioni.length+" opzioni";

        this.say(phrase);

        //update status
        this.setState({
            menuLabel: 'Main',
            menu: opzioni,
            menuCurrentSel: 0,
        })
        //this.readCurrentMenuOption();
        this.say(opzioni[0]);//read first option
    }

    /*Shows and reads date and time*/
    handleDateTime(){
        this.state.textToSpeech.cancel();//stops any other voice playing
        this.props.handleReset();
        this.reset();
        console.log("DateTime requested");
        var dataOra = new Date();
        let finale = 'Time: ' + dataOra.getHours() + ':' + dataOra.getMinutes();
        finale += '\nDate: ' + dataOra.getDate() + '/' + (dataOra.getMonth() + 1) + '/' + dataOra.getFullYear();

        console.log(finale);
        this.props.printTesto(finale);

        let phrase = "Data e ora";
        this.say(phrase);
    }

    //show book list
    listBooks(){
        this.props.handleReset();
        console.log('lista libri');

        //Remote DB connection version
        let listaLibriStampabile = '';
        let libraryMenu = '';

        let emailAddress = this.props.email;
        if(this.props.email===''){
            emailAddress='anon';
        }

        Axios.post(this.props.middlewareAddress+"/getBookList", {
            email: emailAddress,
        }).then((response)=>{
            console.log(response.data);
            this.setState({
                loading: false,
                listBooks: response.data,
            });
            
            for(let i = 0; i<response.data.length; i++){
                if(this.state.showBookId!==0){
                    listaLibriStampabile+='id: '+response.data[i].id+'\n';
                }
                listaLibriStampabile+=response.data[i].title+'\n';
                listaLibriStampabile+=response.data[i].author+'\n';
                listaLibriStampabile+=response.data[i].genre+'\n';
                listaLibriStampabile+=response.data[i].year+'\n';
                listaLibriStampabile+='-\n';//SEPARATORE
    
                //building the menu
                if(i===response.data.length-1){
                    //the last one doesn't need the line break
                    libraryMenu += response.data[i].title;
                    libraryMenu += '\t'+response.data[i].id;
                } else {
                    libraryMenu += response.data[i].title;
                    libraryMenu += '\t'+response.data[i].id+'\n';
                }
            }
            this.generateSubMenu(libraryMenu, 'library');
            this.props.printTesto(listaLibriStampabile);
        });
    }

    //Genera requested submenu with its label
    generateSubMenu(submenu, label) {
        let nuovoMenu =  submenu.split('\n');

        this.props.handleReset();
        this.props.printTesto(submenu);

        this.say(nuovoMenu.length+" opzioni");
        this.say(nuovoMenu[0]);//read first option
        
        this.setState({
            menuLabel: label,
            menu: nuovoMenu,
            menuCurrentSel: 0,
        });
    }

    //confirm button
    handleConfirm(){
        this.state.textToSpeech.cancel();//stops any other voice playing
        let settingsSubMenu;

        console.log("confirm pressed");

        switch(this.state.menuLabel){
            case 'Main'://Menu principale

                switch(this.state.menu[this.state.menuCurrentSel]){
                    case 'Sperimenta':
                        this.say("Confermato sperimenta");
        
                        this.props.handleReset();
                        this.reset();
                        //'sperimenta' option brings the user back to the homepage
                        //That's becausa 'sperimenta' is the default mode of the simulator
                        //(the one where there is only one braille blank page and it's
                        //possible to write in the textarea and upload files)
                    break;
                    case 'Libreria':
                        this.say("Confermato libreria");

                        //print list of available books
                        this.listBooks();
        
                    break;
                    case 'Impostazioni':
                        this.say("Confermato impostazioni");
        
                        //print settings submenu
                        settingsSubMenu = "Configura velocità voce\nPreferenze libreria";
                        this.generateSubMenu(settingsSubMenu, 'Settings');
        
                    break;
                    case 'Informazioni dispositivo':
                        this.say("Confermato informazioni dispositivo");
        
                        //print device information submenu
                        settingsSubMenu = "Spazio di Archiviazione\nVersione sistema\nInformazioni sviluppatore\nLicenze";
                        this.generateSubMenu(settingsSubMenu, 'Device info');
        
                    break;
                    default:
                        this.say('La voce del menu selezionata non esiste"');
                    break;
                }

            break;
            case 'Settings':

                switch(this.state.menu[this.state.menuCurrentSel]){
                    case 'Configura velocità voce':
                        this.say("Premi i tasti su o giù per aumentare o ridurre la velocità della voce");
                        
                        //allows to configure voice reading speed
                        //see code in the up and down buttons

                        //update label
                        this.setState({
                            menuLabel: 'voice speed confirmed',
                        });
        
                    break;
                    case 'Preferenze libreria':
                        this.say("Premi su per mostrare l'id dei libri nella tua libreria, premi giù altrimenti");

                        //update label
                        this.setState({
                            menuLabel: 'set library id',
                        });
                        //allows to choose to show book id or not
                        //see code in the up and down buttons
                    break;
                    default:
                        this.say('La voce del menu selezionata non esiste"');
                    break;
                }
            break;
            case 'Device info':

                switch(this.state.menu[this.state.menuCurrentSel]){
                    case 'Spazio di Archiviazione':
                        this.say("Confermato Spazio di Archiviazione");
        
                        this.say("Stai usando la versione web del simulatore: i dati mostrati sono a solo scopo dimostrativo");

                        let spazioDisp = "Usati 1.2 Gigabyte di 4 totali";

                        this.say(spazioDisp);

                        this.props.handleReset();
                        this.props.printTesto(spazioDisp);
        
                    break;
                    case 'Versione sistema':
                        this.say("Confermato Versione sistema");

                        let osVer = '0.1 alpha';
                        let versione = "Stai usando la versione "+osVer+" di eBraille OS";

                        this.say(versione);

                        this.props.handleReset();
                        this.props.printTesto(versione);
        
                    break;
                    case 'Informazioni sviluppatore':
                        this.say("Confermato Informazioni sviluppatore");

                        let sviluppatore = "Sistema sviluppato da Andrea Munari come tesi di laurea triennale in Ingegneria Informatica. Relatore: Prof. Paolo Bellavista";

                        this.say(sviluppatore);

                        this.props.handleReset();
                        this.props.printTesto(sviluppatore);

                    break;
                    case 'Licenze':
                        this.say("Confermato Licenze");

                        let licenze = "Tutti i libri caricati nel simulatore sono di pubblico dominio."

                        this.say(licenze);

                        this.props.handleReset();
                        this.props.printTesto(licenze);

                    break;
                    default:
                        this.say('La voce del menu selezionata non esiste"');
                    break;
                }
                
            break;
            case 'library':
                //db version
                let libro = this.state.menu[this.state.menuCurrentSel];
                let pieces = libro.split('\t');
                let indice = pieces[1].trim();

                Axios.post(this.props.middlewareAddress+"/downloadBook", {
                    book: indice,
                }).then((response)=>{

                    console.log(response.data[0]);

                    this.props.handleReset();
                    this.props.printTesto(response.data[0].book_file);
                });

            break;
            default:
                this.say('La voce del menu selezionata non esiste"');
            break;
        }
    }

    //Down button
    handleDown(){
        this.state.textToSpeech.cancel();//stops any other voice playing
        console.log("down pressed");

        if(this.state.menuLabel==='voice speed confirmed'){
            
            let updatedVoiceConfig = this.state.voiceConfig;
            if(updatedVoiceConfig===0){
                this.say("Sei già alla velocità minore");
            } else {
                updatedVoiceConfig = this.state.voiceConfig-1;//pitch a default
            }
            this.configureVoice(updatedVoiceConfig);

        } else if (this.state.menuLabel === 'set library id') {
            
            if(this.state.showBookId===0){
                this.say("L'id è già nascosto");
            } else {
                this.setState({
                    showBookId: 0,
                });
                this.say("L'id è ora nascosto dalla libreria");
            }

        } else {
            let nuovaPos = this.state.menuCurrentSel+1;

            if(nuovaPos>=this.state.menu.length){
                nuovaPos=0;//circular menu
                //nuovaPos=this.state.menu.length;//NOT circular menu
            }
            this.setState({
                menuCurrentSel: nuovaPos,
            });
            this.say(this.state.menu[nuovaPos]);
        }
    }

    //Up button
    handleUp(){
        this.state.textToSpeech.cancel();//stops any other voice playing
        console.log("up pressed");
        
        if(this.state.menuLabel==='voice speed confirmed'){
            
            let updatedVoiceConfig = this.state.voiceConfig;
            
            updatedVoiceConfig = this.state.voiceConfig+1;//default pitch
            
            this.configureVoice(updatedVoiceConfig);

        } else if (this.state.menuLabel === 'set library id') {

            if(this.state.showBookId===1){
                this.say("L'id viene già mostrato nella libreria");
            } else {
                this.setState({
                    showBookId: 1,
                });
                this.say("L'id è ora visibile nella libreria");
            }
        } else {
            let nuovaPos = this.state.menuCurrentSel-1;
            if(nuovaPos<0){
                //nuovaPos=0;//NOT circular menu
                nuovaPos=this.state.menu.length-1;//circular menu
            }
            this.setState({
                menuCurrentSel: nuovaPos,
            });
            this.say(this.state.menu[nuovaPos]);
        }
    }

    render(){
        let loader;

        if(this.state.loading){
            loader = (<div className="spinner-border" role="status">
                        <span className="sr-only"></span>
                    </div>);
        }

        return(
            <>  
                <div className="row">
                    <div className="col-sm">

                    </div>
                    <div className="col-sm">
                        <button type="button" className="btn btn-outline-dark" aria-label="Go up" alt='up' onClick={this.handleUp} data-bs-toggle="tooltip" data-bs-placement="top" title="Go up">
                            <i className="bi bi-arrow-up"></i>
                        </button>
                    </div>
                    <div className="col-sm">
                        
                    </div>
                </div>
                <div className="row">
                    <div className="col-sm">
                        <button type="button" className="btn btn-outline-dark" aria-label="Go to previous page" alt='left' onClick={this.props.handleBack} data-bs-toggle="tooltip" data-bs-placement="top" title="Go to previous page">
                            <i className="bi bi-arrow-left"></i>
                        </button>
                    </div>
                    <div className="col-sm">
                        <span className='align-middle'>{this.props.numPagAttuale+1}/{this.props.numPagine}</span>
                    </div>
                    <div className="col-sm">
                        <button type="button" className="btn btn-outline-dark" aria-label="Go to next page" alt='right' onClick={this.props.handleForward} data-bs-toggle="tooltip" data-bs-placement="top" title="Go to next page">
                            <i className="bi bi-arrow-right"></i>
                        </button>
                    </div>
                </div>
                <div className="row">
                    <div className="col-sm">

                    </div>
                    <div className="col-sm">
                        <button type="button" className="btn btn-outline-dark" aria-label="Go down" alt='down' onClick={this.handleDown} data-bs-toggle="tooltip" data-bs-placement="top" title="Go down">
                            <i className="bi bi-arrow-down"></i>
                        </button>
                    </div>
                    <div className="col-sm">
                        
                    </div>
                </div>
                <br/>
                <div className='row'>
                    <div className="col-sm">
                        <button type="button" className="btn btn-outline-dark" aria-label="Go to main menu" alt='main menu' onClick={this.handleMenu} data-bs-toggle="tooltip" data-bs-placement="top" title="Go to main menu">
                            <i className="bi bi-list"></i>
                        </button>
                    </div>
                    <div className="col-sm">
                        <button type="button" className="btn btn-outline-dark" aria-label="Confirm" alt='confirm' onClick={this.handleConfirm} data-bs-toggle="tooltip" data-bs-placement="top" title="Confirm">
                            <i className="bi bi-check-lg"></i>
                        </button>
                    </div>
                    <br /><br />
                    <div className='col-sm'>
                        <button  type="button" className="btn btn-outline-dark" aria-label="Show date and time" alt='date and time' onClick={this.handleDateTime} data-bs-toggle="tooltip" data-bs-placement="top" title="Show current date and time">
                            <i className="bi bi-watch"></i>
                        </button>
                    </div>
                </div>
                {loader}
            </>
        );
    }
}

export default PageControls;