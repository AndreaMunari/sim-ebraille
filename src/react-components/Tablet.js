import LetteraBraille from "./LetteraBraille";
import 'bootstrap/dist/css/bootstrap.css';
import "bootstrap-icons/font/bootstrap-icons.css";
import React from 'react';
import PageControls from "./PageControls";
import UserInput from "./UserInput";
import UploadFileToServer from "./UploadFileToServer";

class Tablet extends React.Component {

    constructor(props) {
        super(props);
        let randomString = Math.random().toString(36);
        let randomStringUpload = Math.random().toString(36);

        this.state = {
          value: '',
          fileValue: '',
          fileUploadValue: '',//
          inputKey: randomString,
          inputUploadKey: randomStringUpload,//
          lettere: [],
          numLetterePerPag: 40,//numero di simboli braille per pagina
          numPagine: 1,
          numPagAttuale: 0,
          tradotto: '',
        };
    
        this.handleChange = this.handleChange.bind(this);
        this.handleReset = this.handleReset.bind(this);
        this.handleStampa = this.handleStampa.bind(this);
        
        this.printTesto = this.printTesto.bind(this);
        this.printPage = this.printPage.bind(this);

        this.printCarattere = this.printCarattere.bind(this);
        this.printCarattereOn = this.printCarattereOn.bind(this);
        this.handleBack = this.handleBack.bind(this);
        this.handleForward = this.handleForward.bind(this);
        this.onFileUpdate = this.onFileUpdate.bind(this);

        this.onUploadFileUpdate = this.onUploadFileUpdate.bind(this);

    }

    //initialize page with blank Braille chars
    componentDidMount(){
        for(let pos = 0; pos<this.state.numLetterePerPag; pos++){
            this.printCarattere(pos, ' ', ' ');
        }
    }

    //reset both the page and the array
    handleReset(){
        let resettato = [[]];
        let randomString = Math.random().toString(36);
        let randomStringUpload = Math.random().toString(36);

        for(let pos = 0; pos<this.state.numLetterePerPag; pos++){
            this.printCarattere(pos, ' ', ' ');
        }

        this.setState({
            nextLettere: resettato,
            prevLettere: resettato,
            inputKey: randomString,
            inputUploadKey: randomStringUpload, 
            value: '',
            fileValue: '',
            fileUploadValue: '',
            numPagine: 1,
            numPagAttuale: 0,
        })
    }

    //like a reset, but doesn't remove content from textarea
    softReset(){
        let resettato = [[]];
        let randomString = Math.random().toString(36);
        let randomStringUpload = Math.random().toString(36);

        for(let pos = 0; pos<this.state.numLetterePerPag; pos++){
            this.printCarattere(pos, ' ', ' ');
        }

        this.setState({
            nextLettere: resettato,
            prevLettere: resettato,
            inputKey: randomString,
            inputUploadKey: randomStringUpload, 
            fileValue: '',
            fileUploadValue: '',
            numPagine: 1,
            numPagAttuale: 0,
        })
    }

    handleChange(event) {
        this.setState({value: event.target.value});
    }

    handleStampa(event) {
        this.softReset();
        let testo;

        if(event===undefined){
            
        } else {
            if(this.state.fileValue==='' || this.state.fileValue===undefined){
                testo = this.state.value;
            } else {
                testo = this.state.fileValue;
            }
            console.log(testo);
            this.printTesto(testo);
        }
        
    }

    isNumber(str) {
        let numeri = ['1','2','3','4','5','6','7','8','9','0']; 
        
        return numeri.includes(str);
    }

    isUpper(str){
        return !/[a-z]/.test(str) && /[A-Z]/.test(str);
    }

    // algorithm to convert input text in formatted string
    //(adds, when necessary, a single non-stampable char to match
    //char count as braille output)
    //Also turn line break char into n blank chars, where n is the number
    //of braille cells before the line break
    convert(testo){
        const carPerRiga = 5;//numero caratteri Braille su una riga
        let lunghezzaIniziale = testo.length;
        let res = '';
        let prec = '';
        
        for(let i = 0; i<lunghezzaIniziale; i++){
            if(i===0){
                prec=' ';
            } else {
                prec = testo[i-1];
            }

            if(this.isNumber(testo[i]) && !this.isNumber(prec)){
                res+='ぁ';//symbol to mark a 'segnanumero'
            } else if (this.isUpper(testo[i])){
                res+='ぅ';//symbol to mark a 'segnamaiuscolo'
            } else if (testo[i]==='~') {
                res+='기';//symbol to mark a 'segna-tilde'
            } else if (testo[i]==='\n'){//if line break
                let daAggiungere = res.length%carPerRiga;
                if(daAggiungere!==0){//text length is not divisible by 5
                    for(let h = 0; h<(carPerRiga-daAggiungere-1); h++){
                        res+=' ';
                    }
                }
            }
            res+=testo[i];
        }
        return res;
    }

    //printTesto calls printPage at page 0 after converting the raw text.
    printTesto(testo) {
        let res = this.convert(testo);
        
        let numPag = (res.length)/this.state.numLetterePerPag;
        numPag = Math.ceil(numPag);

        this.setState({
            numPagine: numPag,
            tradotto: res,
        }, () => {
            this.printPage(0);
        });
    }

    //prints testoTradotto on page numPagina based on converted text
    printPage(numPagina) {
        if(this.state.tradotto===undefined || this.state.tradotto===''){
            console.log("TESTO UNDEFINED");   
        } else { 
            if(numPagina===0) {
                let pagina = [];

                for(let pos = 0; pos<this.state.numLetterePerPag; pos++){
                        
                    this.printCarattereOn(pos, this.state.tradotto[pos], pagina);
                    
                    //fill the rest of the page with blank braille characters
                    if(pos>=this.state.tradotto.length){
                        this.printCarattereOn(pos, ' ', pagina);
                    }
                }

                this.setState({
                    lettere: pagina,
                });

            } else {
                let pagina = [];
                
                let startingPos = this.state.numLetterePerPag*numPagina;

                for(let pos = startingPos; pos<(startingPos+this.state.numLetterePerPag); pos++){
                                        
                    this.printCarattereOn(pos, this.state.tradotto[pos], pagina);
                    
                    //fill the rest of the page with blank braille characters
                    if(pos>=this.state.tradotto.length){
                        this.printCarattereOn(pos, ' ', pagina);
                    }
                }

                this.setState({
                    lettere: pagina,
                });
            }
        }
    }

    printCarattere(pos, carattere){
        let nuovoLettere = this.state.lettere; 
        this.printCarattereOn(pos, carattere, nuovoLettere);

        this.setState({
            lettere: nuovoLettere,
        })
    }

    printCarattereOn(pos, carattere, vettore){
        vettore[pos] = <LetteraBraille key={pos} id={pos} lettera={carattere} />;
    }

    // Callback from <input type="file" onchange="onFileUpdate(event)">
    onFileUpdate(event) {
        var file = event.target.files[0];
        var reader = new FileReader();
        reader.onload = e => {
            // The file's text will be printed here
            console.log(e.target.result);
            this.setState({
                fileValue: e.target.result,
            });
        };
    
        if(file){
            reader.readAsText(file);
        }
    }

    onUploadFileUpdate(event) {
        var file = event.target.files[0];
        var reader = new FileReader();
        reader.onload = e => {
            // The file's text will be printed here
            console.log(e.target.result);
            this.setState({
                fileUploadValue: e.target.result,
            });
        };
    
        if(file){
            reader.readAsText(file);
        }
        
    }

    handleBack(){
        console.log("Back pressed");
        //console.log(this.state.prevLettere);
        
        //if not at the first page, it is possible to go back
        if(this.state.numPagAttuale>0){
            
            let newNumPagAttuale = this.state.numPagAttuale-1;

            this.printPage(newNumPagAttuale);
            console.log("andato indietro");

            this.setState({
                numPagAttuale: newNumPagAttuale,
            });
        } else {
            console.log("Sei alla prima pagina");
        }
    }

    handleForward(){
        
        //if there is still at least one page to show
        if(this.state.numPagAttuale<this.state.numPagine-1){
            
            let newNumPagAttuale = this.state.numPagAttuale+1;

            this.printPage(newNumPagAttuale);
            console.log("andato avanti");

            this.setState({
                numPagAttuale: newNumPagAttuale,
            });
        } else {//otherwise you're at the last page
            console.log("Sei all'ultima pagina");
        }
    }

    render() {
        return (
            <div className="row">
                <div className='col-md-8'>
                    <div className="tablet">
                        
                        {this.state.lettere.map(item => {
                            return <span key={item.id}>{item}</span>;
                        })}
                        <br />

                    </div>
                </div>
                <div className="col-md text-center">                 
                    <UserInput 
                        value={this.state.value}
                        handleChange={this.handleChange}
                        inputKey={this.state.inputKey}
                        onFileUpdate={this.onFileUpdate}
                        handleStampa={this.handleStampa}
                        handleReset={this.handleReset}
                    />
                    <br />
                    {this.props.loggedIn ? 
                    <UploadFileToServer
                        loggedIn={this.props.loggedIn}
                        setLoggedIn={this.props.setLoggedIn}
                        setLoggedOut={this.props.setLoggedOut}
                        fileUploadValue={this.state.fileUploadValue}
                        onUploadFileUpdate={this.onUploadFileUpdate}

                        handleBack={this.handleBack}
                        handleForward={this.handleForward}
                        handleMenu={this.handleMenu}
                        handleConfirm={this.handleConfirm}
                        handleUp={this.handleUp}
                        handleDown={this.handleDown}

                        handleReset={this.handleReset}
                        printTesto={this.printTesto}
                        email={this.props.email}

                        middlewareAddress={this.props.middlewareAddress}
                    />
                    :
                    <div></div>
                    }
                    <br /><br />

                    <PageControls
                        numPagAttuale={this.state.numPagAttuale}
                        numPagine={this.state.numPagine}
                        handleBack={this.handleBack}
                        handleForward={this.handleForward}
                        handleMenu={this.handleMenu}
                        handleConfirm={this.handleConfirm}
                        handleUp={this.handleUp}
                        handleDown={this.handleDown}

                        handleReset={this.handleReset}
                        printTesto={this.printTesto}

                        loggedIn={this.props.loggedIn}
                        email={this.props.email}

                        middlewareAddress={this.props.middlewareAddress}
                    />
                </div>
            </div>
        );
    }
}

export default Tablet;