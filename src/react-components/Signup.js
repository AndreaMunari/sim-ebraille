import 'bootstrap/dist/css/bootstrap.css';
import "bootstrap-icons/font/bootstrap-icons.css";
import { Modal, Button, Form, Alert } from "react-bootstrap";
import Axios from 'axios';
import React from 'react';

class Signup extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            isOpen: false,
            email: '',
            password: '',
            signupError: '',
            showAlert: false,
        };

        this.signup = this.signup.bind(this);
        this.openModal = this.openModal.bind(this);
        this.closeModal = this.closeModal.bind(this);
        this.onEmailChange = this.onEmailChange.bind(this);
        this.onPasswordChange = this.onPasswordChange.bind(this);
        this.setShowAlert = this.setShowAlert.bind(this);
    }

    //makes request and checks if user can signup
    signup() {
        let email = this.state.email;
        let password = this.state.password;

        if (email === '' || password === '') {
            this.setState({
                signupError: 'Remember to fill in all the fields',
                showAlert: true,
            });
        } else {
            Axios.post(this.props.middlewareAddress + "/signup", {
                email: this.state.email,
                password: this.state.password
            }).then((response) => {
                //alert(response.data);
                if (response.data === 'Success') {

                    this.props.setLoggedIn(email);

                } else {
                    this.setState({
                        signupError: response.data,
                        showAlert: true,
                    });
                }
            });
        }


    }

    setShowAlert(open) {

        this.setState({
            showAlert: open,
        });
    }

    openModal() {
        this.setState({
            isOpen: true,
        });
    }

    closeModal() {
        this.setState({
            isOpen: false,
        });
    }

    onEmailChange(event) {
        this.setState({
            email: event.target.value,
        });
    }

    onPasswordChange(event) {
        this.setState({
            password: event.target.value,
        });
    }

    render() {
        return (
            <>
                <Button variant="dark" onClick={this.openModal}>
                    Sign Up
                </Button>

                <Modal show={this.state.isOpen} centered onHide={this.closeModal}>
                    <Modal.Header closeButton>
                        <Modal.Title>Sign Up</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <Form>
                            <Form.Group className="mb-3" controlId="formBasicEmail">
                                <Form.Label>Email address</Form.Label>
                                <Form.Control type="email" placeholder="Enter email" onChange={this.onEmailChange} />
                                <Form.Text className="text-muted">
                                    We'll never share your email with anyone else.
                                </Form.Text>
                            </Form.Group>

                            <Form.Group className="mb-3" controlId="formBasicPassword">
                                <Form.Label>Password</Form.Label>
                                <Form.Control type="password" placeholder="Password" onChange={this.onPasswordChange} />
                            </Form.Group>

                            <Alert variant="danger" show={this.state.showAlert} onClose={() => this.setShowAlert(false)} dismissible>
                                <Alert.Heading>Sign up error!</Alert.Heading>
                                <p>
                                    {this.state.signupError}
                                </p>
                            </Alert>

                            <Button variant="dark" onClick={this.signup}>
                                Submit
                            </Button>

                        </Form>
                    </Modal.Body>
                </Modal>
            </>
        );
    }
}

export default Signup;