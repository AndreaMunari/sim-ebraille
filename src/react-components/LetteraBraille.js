import React from 'react';

class LetteraBraille extends React.Component {

    isUpper(str){
        return !/[a-z]/.test(str) && /[A-Z]/.test(str);
    }

    isLetter(str){
        let lettere = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyzèéùàò";
        return lettere.includes(str);
    }

    isNumber(str){
        let numeri = ['1','2','3','4','5','6','7','8','9','0']; 
        return numeri.includes(str);
    }

    render() {
        let lettera = this.props.lettera;
        let letteraOK = lettera;
        let immagineRichiesta = '../imgsBraille/';

        if(this.isLetter(lettera)){//if letter, it is printed immediately
            immagineRichiesta += lettera.toUpperCase();

        } else if (this.isNumber(lettera)){//if number, convert to letter

            let convertita = ' ';

            if(lettera==='1'){
                convertita = 'a';
            } else if(lettera==='2'){
                convertita = 'b';
            } else if(lettera==='3'){
                convertita = 'c';
            } else if(lettera==='4'){
                convertita = 'd';
            } else if(lettera==='5'){
                convertita = 'e';
            } else if(lettera==='6'){
                convertita = 'f';
            } else if(lettera==='7'){
                convertita = 'g';
            } else if(lettera==='8'){
                convertita = 'h';
            } else if (lettera==='9') {
                convertita = 'i';
            } else if (lettera==='0') {
                convertita = 'j';
            }

            immagineRichiesta += convertita;

        } else {//if symbol

            if(lettera==='ぁ'){//segnanumero
                immagineRichiesta += 'segna-numero';
                letteraOK = 'segna-numero';
            } else if (lettera==='ぅ') {//segnamaiuscolo
                immagineRichiesta += 'maiuscolo';
                letteraOK = 'maiuscolo';
            } else if (lettera==='기') {//segnatilde
                immagineRichiesta += 'segna-tilde';
                letteraOK = 'segna-tilde';

            //Classic symbols:
            } else if(lettera === ' ' || lettera=== '\n' || lettera==='\r'){
                immagineRichiesta +='vuoto';
            } else if(lettera === "'" || lettera === '"'|| lettera==='`'){//show ' even if input is " or `
                immagineRichiesta +='apostrofo';
            } else if(lettera === '*') {
                immagineRichiesta +='asterisco';
            } else if (lettera === ':'){
                immagineRichiesta +='duepunti';
            } else if (lettera==='!') {
                immagineRichiesta +='esclamativo';
            } else if (lettera==='?') {
                immagineRichiesta +='interrogativo';
            } else if (lettera==='>') {
                immagineRichiesta +='maggiore';
            } else if (lettera==='<') {
                immagineRichiesta +='minore';
            } else if(lettera==='*'){
                immagineRichiesta +='per';
            } else if(lettera==="."){
                immagineRichiesta +='punto';
            } else if (lettera === ';') {
                immagineRichiesta +='puntoevirgola';
            } else if (lettera === ',') {
                immagineRichiesta +='virgola';
            } else if (lettera==='«') {
                immagineRichiesta +='virgolettebasseAPERTE';
            } else if (lettera==='»'){
                immagineRichiesta +='virgolettebasseCHIUSE';
            } else if (lettera==='/' || lettera==='\\'){//left or right dash is always /
                immagineRichiesta +='barra';
            } else if (lettera==='-') {
                immagineRichiesta +='dash';
            } else if (lettera==='{') {
                immagineRichiesta +='{';
            } else if (lettera==='}') {
                immagineRichiesta +='}';
            } else if (lettera==='=') {
                immagineRichiesta +='=';
            } else if (lettera==='#'){
                immagineRichiesta +='segna-numero';
            } else if (lettera==='[') {
                immagineRichiesta +='[';
            } else if (lettera===']') {
                immagineRichiesta +=']';
            } else if (lettera==='(' || lettera==='~') {
                immagineRichiesta +='(';
            } else if (lettera===')') {
                immagineRichiesta +=')';
            }
        }//end of the translation

        //set file extension
        immagineRichiesta += '.webp';
        
        return (
            <>
                <img src={immagineRichiesta} key={immagineRichiesta.id} className="letteraBraille" alt={letteraOK} />
            </>
        );
    }
}

export default LetteraBraille;