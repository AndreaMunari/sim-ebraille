import 'bootstrap/dist/css/bootstrap.css';
import "bootstrap-icons/font/bootstrap-icons.css";
import { Modal, Button, Form, Alert } from "react-bootstrap";
import Axios from 'axios';
import React from 'react';

class UploadFileToServer extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            isOpen: false,
            fileUploadError: '',
            showAlert: false,
            showSuccessfulUpdateAlert: false,
            numUploads: 0,
            titolo: '',
            autore: '',
            genere: '',
            anno: '',
        };
    
        this.openModal = this.openModal.bind(this);
        this.closeModal = this.closeModal.bind(this);
        this.setShowAlert = this.setShowAlert.bind(this);
        this.setShowSuccessfulUpdateAlert = this.setShowSuccessfulUpdateAlert.bind(this);

        this.onTitleChange = this.onTitleChange.bind(this);
        this.onAuthorChange = this.onAuthorChange.bind(this);
        this.onGenreChange = this.onGenreChange.bind(this);
        this.onYearChange = this.onYearChange.bind(this);

        this.handleFileUpload = this.handleFileUpload.bind(this);
        this.updateNumUploads = this.updateNumUploads.bind(this);
    }

    onTitleChange(event){
        this.setState({
            titolo: event.target.value,
        });
    }

    onAuthorChange(event) {
        this.setState({
            autore: event.target.value,
        });
    }

    onGenreChange(event) {
        this.setState({
            genere: event.target.value,
        });
    }

    onYearChange(event) {
        this.setState({
            anno: event.target.value,
        });
    }

    componentDidMount(){
        this.updateNumUploads();
    }

    //queries the DB about how many books the user uploaded, and save it in the state.numUploads
    updateNumUploads(){
        let emailToSend = this.props.email;

        Axios.post(this.props.middlewareAddress+"/getUserUploadNumber", {
            email: emailToSend,
        }).then((response) => {
            
            if(response.data.info === 'Success'){
                //upload successful
                
                this.setState({
                    numUploads: response.data.uploadNumber,
                });

            } else {
                //Upload unsuccessful
                this.setState({
                    fileUploadError: response.data,
                    showAlert: true,
                });
            }
        });
    }

    openModal() {
        this.updateNumUploads();
        this.setState({
            isOpen: true,
        });
    }

    setShowAlert(open) {
        this.setState({
            showAlert: open,
        });
    }

    setShowSuccessfulUpdateAlert(open){
        this.setState({
            showSuccessfulUpdateAlert: open,
        });
    }

    closeModal() {
        this.setState({
            isOpen: false,
        });
    }

    handleFileUpload(){
        this.updateNumUploads();//

        let emailToSend = this.props.email;
        let titolo = this.state.titolo;
        let autore = this.state.autore;
        let genere = this.state.genere;
        let anno = this.state.anno;
        let fileToUpload = this.props.fileUploadValue;

        //upload file to server
        console.log("Test");
        console.log(this.props.fileUploadValue);
        if(fileToUpload===''){
            //didn't upload file
            this.setState({
                fileUploadError: "You have to choose a file to upload",
                showAlert: true,
            });

        }

        if(this.state.numUploads>=3){
            //book upload limit exceeded
            this.setState({
                fileUploadError: "You can't upload more books",
                showAlert: true,
            });
        }

        if (titolo==='' || autore==='' || genere==='' || anno==='') {
            this.setState({
                fileUploadError: "Remeber to fill in all the fields",
                showAlert: true,
            });
        } else {
            Axios.post(this.props.middlewareAddress+"/uploadBook", {
                email: emailToSend,
                title: titolo,
                author: autore,
                genre: genere,
                year: anno,
                file: fileToUpload,
            }).then((response) => {
                if(response.data === 'Success'){
                    //upload successful
                    this.setShowSuccessfulUpdateAlert(true);

                } else {
                    //Upload unsuccessful
                    this.setState({
                        fileUploadError: response.data,
                        showAlert: true,
                    });
                }
            });
        }
    }

    render(){
        let remaining = <div></div>;
        let showInputFile = true;

        if(this.state.numUploads===-1){
            remaining = <Alert variant="danger">
                            <Alert.Heading>Backend error</Alert.Heading>
                            <p>
                                Try again later
                            </p>
                        </Alert>
            showInputFile = false;
        } else {
            let uploadsLeft = 3-this.state.numUploads;
            if(uploadsLeft>0 && uploadsLeft!==1) {
                remaining = <Alert variant="info">
                                <Alert.Heading>You have {uploadsLeft} book uploads left.</Alert.Heading>
                            </Alert>
            } else if (uploadsLeft===0) {
                //user already uploaded 3 books
                remaining = <Alert variant="info">
                                <Alert.Heading>You don't have any book uploads left.</Alert.Heading>
                            </Alert>
                showInputFile = false;
            } else if (uploadsLeft===1) {
                //last book upload
                remaining = <Alert variant="info">
                                <Alert.Heading>You have only {uploadsLeft} book upload left.</Alert.Heading>
                            </Alert>
            }
        }

        return(
            <>
                <Button variant="outline-dark" onClick={this.openModal}>
                    Upload book to server
                </Button>
                <Modal show={this.state.isOpen} centered onHide={this.closeModal}>
                    <Modal.Header closeButton>
                        <Modal.Title>Upload book to server</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                            <Alert variant="warning">
                                <Alert.Heading>Do NOT upload copyrighted material!</Alert.Heading>
                                <p>
                                    eBraille cannot be considered legally responsible for any file uploaded by the user.<br/>
                                    If you upload files to our platform without having the right to redistribute those files, you may run into legal problems.
                                </p>
                            </Alert>
                        <Form>
                            {remaining}
                            {showInputFile ? 
                            <>
                                <Form.Group className="mb-3" controlId="formUploadFile">
                                    <Form.Label>Choose a book to upload</Form.Label>
                                    <Form.Control
                                        type="file"
                                        accept="text/plain"
                                        key={this.props.inputUploadKey || ''}
                                        placeholder="Enter email"
                                        onChange={this.props.onUploadFileUpdate}
                                        onClick={e => (e.target.value = null)}
                                    />

                                    <Form.Label>Title</Form.Label>
                                    <Form.Control type="text" placeholder="Title" onChange={this.onTitleChange}/>

                                    <Form.Label>Author</Form.Label>
                                    <Form.Control type="text" placeholder="Author" onChange={this.onAuthorChange}/>

                                    
                                    <Form.Label>Genre</Form.Label>
                                    <Form.Control type="text" placeholder="Genre" onChange={this.onGenreChange}/>

                                    
                                    <Form.Label>Year</Form.Label>
                                    <Form.Control type="text" placeholder="Year" onChange={this.onYearChange}/>
                                </Form.Group>

                                <Button variant="dark" onClick={this.handleFileUpload}>
                                    Submit
                                </Button>
                            </>
                            :
                            <div></div>
                            }
                            
                            <Alert variant="danger" show={this.state.showAlert} onClose={() => this.setShowAlert(false)} dismissible>
                                <Alert.Heading>Upload error!</Alert.Heading>
                                <p>
                                {this.state.fileUploadError}
                                </p>
                            </Alert>

                            <Alert variant="success" show={this.state.showSuccessfulUpdateAlert} onClose={() => this.setShowSuccessfulUpdateAlert(false)} dismissible>
                                <Alert.Heading>File uploaded succesfully</Alert.Heading>
                            </Alert>

                        </Form>
                    </Modal.Body>
                </Modal>
            </>
        );
    }
}

export default UploadFileToServer;