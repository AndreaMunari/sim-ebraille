import 'bootstrap/dist/css/bootstrap.css';
import "bootstrap-icons/font/bootstrap-icons.css";
import React from 'react';


class UserInput extends React.Component {

    render(){
        return(
            <>
                <div className="form">
                    <label>
                        <h4>Scrivi qualcosa nella casella...</h4>
                        <textarea className="form-control" value={this.props.value} onChange={this.props.handleChange}  />
                    </label>
                    <label>
                        <h4>...oppure carica un file di testo:</h4>
                        <input type='file' accept="text/plain" key={this.props.inputKey || ''} className='form-control' onChange={this.props.onFileUpdate} onClick={e => (e.target.value = null)}></input>
                    </label>
                    
                </div>

                <br /><br />
                
                <div>
                    <label>
                        <h4>Poi premi Submit:</h4>
                        <input type="button" className="btn btn-primary" value="Submit" onClick={this.props.handleStampa} />
                        <input type="button" className="btn btn-danger" value="Reset" onClick={this.props.handleReset} />
                    </label>
                </div>
            </>
        );
    }

}

export default UserInput;