import 'bootstrap/dist/css/bootstrap.css';
import "bootstrap-icons/font/bootstrap-icons.css";
import {  Button } from "react-bootstrap";
import React from 'react';

class Logout extends React.Component {

    constructor(props) {
        super(props);

        this.logout = this.logout.bind(this);
    }

    logout() {
       console.log('logout');
       this.props.setLoggedOut();
    }

    render(){
        return(
            <>
                <Button variant="dark" onClick={this.logout}>
                    Logout
                </Button>
            </>
        );
    }
}

export default Logout;